import Express, { Application, Request, Response } from "express";
import { existsSync } from "fs";
import { readFile } from "fs/promises";
import morgan = require("morgan");
import { resolve } from "path";
import { component, initialize, inject } from "tsdi";
import { BotConfig } from "./config";
import { Logger } from "./logger";

@component
export class HttpServer {
    @inject private config!: BotConfig;
    @inject private logger!: Logger;

    private app: Application;

    @initialize protected initialize(): void {
        this.app = Express();
        this.app.use(morgan("tiny", { stream: { write: (msg) => this.logger.info(msg.trim()) } }));
        this.app.get("/:fileName", (req, res) => this.onGetRef(req, res));
        this.logger.info(`HTTP server listening on port ${this.config.port}`);
        this.app.listen(this.config.port);
    }

    private async onGetRef(req: Request, res: Response): Promise<void> {
        const path = resolve(this.config.cache, req.params.fileName);
        if (!existsSync(path)) {
            res.status(400).send();
            return;
        }
        res.status(200).contentType("image/jpeg").send(await readFile(path));
    }
}
