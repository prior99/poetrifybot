import { Logger } from "./logger";

export function handlePromises(logger: Logger): void {
    process.on("unhandledRejection", (err: Error) => {
        logger.error(`Unhandled Promise rejection: ${err.message}`);
        console.error(err);
    });
    process.on("uncaughtException", (err: Error) => {
        logger.error(`Unhandled Promise rejection: ${err.message}`);
        console.error(err);
    });
}
