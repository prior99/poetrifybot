import { option, Options } from "clime";

export class BotConfig extends Options {
    @option({ description: "Telegram bot token.", required: true })
    public telegramBotToken!: string;

    @option({ description: "Public url.", required: true })
    public publicUrl!: string;

    @option({ description: "HTTP Port.", default: 9865 })
    public port!: number;

    @option({ description: "Directory for caching images.", default: "cache" })
    public cache!: string;
}
