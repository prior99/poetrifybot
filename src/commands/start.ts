import { Command, command, metadata } from "clime";
import { TSDI } from "tsdi";
import { BotConfig } from "../config";
import { handlePromises } from "../handle-promises";
import { onKill } from "../kill";
import { Logger } from "../logger";
import { Telegram } from "../telegram";
import { HttpServer } from "../http-server";
import { PoetrifyBot } from "../poetrifybot";

@command({ description: "Start the bot." })
export default class StartCommand extends Command {
    @metadata
    public async execute(config: BotConfig): Promise<void> {
        const tsdi = new TSDI(new PoetrifyBot(config));
        await tsdi.asyncGet(Telegram);
        await tsdi.asyncGet(HttpServer);
        onKill(() => tsdi.close(), tsdi.get(Logger));
        handlePromises(tsdi.get(Logger));
    }
}
