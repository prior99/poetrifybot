import { component, inject } from "tsdi";
import * as promises from "fs/promises";
import { BotConfig } from "./config";
import { existsSync } from "fs";
import * as path from "path";
import { Canvas, Image, loadImage, registerFont } from "canvas";
import { createHash } from "crypto";

registerFont(path.join(__dirname, "..", "fonts", "Moms_typewriter.ttf"), { family: "Moms" });

export interface PoemResult {
    readonly content: Buffer;
    readonly hash: string;
}

const padding = 50;

@component
export class Images {
    @inject private readonly config!: BotConfig;

    private drawText(context: CanvasRenderingContext2D, text: string, x: number, y: number, size: number): void {
        context.fillStyle = "rgba(0, 0, 0, 0.2)";
        context.font = `${size}px bold Moms`;
        context.fillText(text, x - 2, y);

        context.fillStyle = "white";
        context.font = `${size}px bold Moms`;
        context.fillText(text, x + 2, y + 4);
    }

    public async getPoems(poem: string, username: string): Promise<PoemResult[]> {
        promises.mkdir(this.config.cache, { recursive: true });

        const imageFiles = await promises.readdir(path.resolve(__dirname, "..", "images"));

        return await Promise.all(
            imageFiles.map(
                async (imageFile) =>
                    await this.getPoem(poem, username, path.resolve(__dirname, "..", "images", imageFile)),
            ),
        );
    }

    private async getPoem(poem: string, username: string, backgroundImageName: string): Promise<PoemResult> {
        const hash = createHash("sha256").update(`${poem}_${backgroundImageName}`).digest("hex").substr(0, 20);
        const fileName = path.join(this.config.cache, `${hash}.jpg`);

        if (existsSync(fileName)) {
            return { hash, content: await promises.readFile(fileName) };
        }

        const lines = (poem.match(/\n/g) || "").length + 1;

        const backgroundImage = await loadImage(backgroundImageName);
        const canvas = new Canvas(1000, 1000);
        const context = await canvas.getContext("2d");

        let lineHeight = Math.min((1000 - padding * 2) / lines / 2, 70);
        do {
            lineHeight = lineHeight - 5;
            context.font = `${lineHeight}px bold Moms`;
        } while (context.measureText(poem).width > 1000 - padding * 2);

        context.textAlign = "center";
        context.drawImage(backgroundImage, 0, 0, 1000, 1000);

        context.fillStyle = "rgba(0, 0, 0, 0.3)";
        context.fillRect(0, 0, 1000, 1000);
        const top = (1000 - (lines * lineHeight + padding * 2)) / 2;

        this.drawText(context, poem, 1000 / 2, top, lineHeight);

        context.textAlign = "right";

        this.drawText(context, `- ${username}`, 1000 - padding, 1000 - 40 - padding, 40);

        const content = canvas.toBuffer("image/jpeg");
        await promises.writeFile(fileName, content);
        return { hash, content };
    }
}
