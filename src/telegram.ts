import { InlineQuery, InlineQueryResultPhoto } from "node-telegram-bot-api";
import TelegramBot from "node-telegram-bot-api";
import { component, initialize, inject } from "tsdi";
import { BotConfig } from "./config";
import { Logger } from "./logger";
import { Images } from "./images";

@component
export class Telegram {
    @inject private readonly logger!: Logger;
    @inject private readonly config!: BotConfig;
    @inject private readonly images!: Images;

    private telegramBot!: TelegramBot;

    @initialize protected async initialize(): Promise<void> {
        this.logger.info("Connecting to telegram.");
        this.telegramBot = new TelegramBot(this.config.telegramBotToken, { polling: true });
        this.telegramBot.on("error", (err) => this.logger.error(`Telegram bot: ${err.message}`, err));
        this.telegramBot.on("polling_error", (err) => this.logger.error(`Telegram bot polling: ${err.message}`, err));
        this.telegramBot.on("inline_query", async (query: InlineQuery) => this.onInlineQuery(query));
    }
    private async onInlineQuery(query: InlineQuery): Promise<void> {
        this.logger.info(`New poem request from ${query.from.username}: ${query.query.replace("\n", " ")}`);
        const poems = await this.images.getPoems(query.query, query.from.username);
        const results = poems.map(({ hash }) => {
            const url = `${this.config.publicUrl}/${hash}.jpg`;
            return {
                type: "photo" as const,
                id: hash,
                thumb_url: url,
                photo_url: url,
                photo_height: 1000,
                photo_width: 1000,
            };
        });
        await this.telegramBot.answerInlineQuery(query.id, results, { cache_time: 60 });
    }
}
